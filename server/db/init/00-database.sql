CREATE DATABASE postgres;

\connect postgres;

CREATE SCHEMA public;
CREATE TABLE public.users (
    id SERIAL PRIMARY KEY,
    username TEXT,
    email TEXT,
    password TEXT,
    createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
COMMENT ON TABLE public.users IS
'Provide a description for your parent table.';
CREATE TABLE public.messages (
    id SERIAL PRIMARY KEY,
    message TEXT,
    userId INTEGER NOT NULL REFERENCES public.users(id)
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
);
COMMENT ON TABLE public.messages IS
'Provide a description for your child table.';