import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { config } from 'dotenv';
import { TypeGraphQLModule } from 'typegraphql-nestjs';
import { MessageModule } from './messages/module';
import { UserModule } from './user/module';

config();

const username = process.env.POSTGRES_USER || 'postgres';
const password = process.env.POSTGRES_PASSWORD || 'example';
const database = process.env.DATABASE || 'postgres';
const CORS_WHITELIST = process.env.CORS_WHITELIST || 'http://localhost:3000';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username,
      password,
      database,
      entities: [__dirname + '/entities/*.{ts,js}'],
      synchronize: true,
      logging: 'all',
    }),
    TypeGraphQLModule.forRoot({
      emitSchemaFile: true,
      validate: false,
      debug: true,
      cors: {
        origin: CORS_WHITELIST,
        credentials: true,
      },
      context: ({ req }) => ({ headers: req.headers }),
    }),
    UserModule,
    MessageModule,
  ],
})
export class AppModule {}
