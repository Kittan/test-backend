import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Message } from '../entities/Message';
import { User } from '../entities/User';

import { MessageResolver } from './resolver';
import { MessageService } from './service';

@Module({
  imports: [TypeOrmModule.forFeature([Message, User])],
  providers: [MessageService, MessageResolver],
})
export class MessageModule {}
