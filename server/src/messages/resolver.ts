import {
  Arg,
  Mutation,
  Query,
  Resolver,
  ID,
  UseMiddleware,
} from 'type-graphql';
import {
  ClientProxy,
  ClientProxyFactory,
  Transport,
} from '@nestjs/microservices';

import { User } from '../entities/User';
import { MessageService } from './service';
import { Message } from '../entities/Message';
import { isAuth } from '../middleware/isAuth';

@Resolver(User)
export class MessageResolver {
  private client: ClientProxy;

  constructor(public service: MessageService) {
    this.client = ClientProxyFactory.create({
      transport: Transport.REDIS,
      options: {
        url: 'redis://localhost:6379',
      },
    });
  }

  @UseMiddleware(isAuth)
  @Query(() => [Message])
  async messages(): Promise<Message[]> {
    return await Message.find({
      relations: ['user'],
      order: {
        createdAt: 'ASC',
      },
    });
  }

  @UseMiddleware(isAuth)
  @Mutation(() => [Message])
  async addMessage(
    @Arg('userId', () => ID) userId: number,
    @Arg('message', () => String) message: string,
  ): Promise<Message[]> {
    const messages = await this.service.addMessage({
      message,
      userId,
    });

    return messages;
  }
}
