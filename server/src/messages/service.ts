import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as Pusher from 'pusher';

import { UserMessageDTO } from '../types';
import { Message } from '../entities/Message';
import { User } from '../entities/User';

@Injectable()
export class MessageService {
  constructor(
    @InjectRepository(Message) private repository: Repository<Message>,
    @InjectRepository(User) private userRepository: Repository<User>,
  ) {}

  async addMessage({ userId, message }: UserMessageDTO) {
    const pusher = new Pusher({
      appId: '1192615',
      key: '2e8f04eee573f2ad5543',
      secret: 'd4049c934cb4e14a81d7',
      cluster: 'eu',
      encrypted: true,
    });
    const user = await this.userRepository.findOne(userId);

    const createdMessage = new Message();

    createdMessage.message = message;
    createdMessage.user = user;

    await this.repository.manager.save(createdMessage);

    pusher.trigger('chats', 'new-chat', createdMessage);

    return await this.repository.find({ relations: ['user'] });
  }
}
