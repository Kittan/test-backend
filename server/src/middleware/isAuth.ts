import { MiddlewareFn } from 'type-graphql';
import * as jwt from 'jsonwebtoken';

import { MyContext, ResultVerify } from '../types';

const SECRET_JWT_KEY = process.env.SECRET_JWT_KEY || 'secret';

export const isAuth: MiddlewareFn<MyContext> = ({ context }, next) => {
  const resultJwt = jwt.verify(
    context.req.headers.authorization.replace('Bearer ', ''),
    SECRET_JWT_KEY,
  ) as ResultVerify;

  if (!resultJwt) {
    throw new Error('not authenticated');
  }

  return next();
};
