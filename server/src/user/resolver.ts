import {
  Arg,
  Ctx,
  Field,
  Mutation,
  ObjectType,
  Query,
  Resolver,
} from 'type-graphql';
import * as argon2 from 'argon2';
import * as jwt from 'jsonwebtoken';

import { User } from '../entities/User';
import { MyContext, ResultVerify } from '../types';
import { validateRegister } from '../utils/validateRegister';
import { UsernamePasswordDTO } from '../types';
import { UserService } from './service';

const SECRET_JWT_KEY = process.env.SECRET_JWT_KEY || 'secret';

@ObjectType()
class FieldError {
  @Field()
  field: string;
  @Field()
  message: string;
}

@ObjectType()
class UserResponse {
  @Field(() => [FieldError], { nullable: true })
  errors?: FieldError[];

  @Field(() => User, { nullable: true })
  user?: User;

  @Field(() => String, { nullable: true })
  token?: string;
}

@Resolver(User)
export class UserResolver {
  constructor(public service: UserService) {}

  @Query(() => User, { nullable: true })
  me(@Ctx() { req }: MyContext) {
    const resultJwt = jwt.verify(
      req.headers.authorization.replace('Bearer ', ''),
      SECRET_JWT_KEY,
    ) as ResultVerify;

    if (!resultJwt) {
      return null;
    }

    return User.findOne(resultJwt.id);
  }

  @Mutation(() => UserResponse)
  async register(
    @Arg('options') options: UsernamePasswordDTO,
  ): Promise<UserResponse> {
    const errors = validateRegister(options);

    if (errors) {
      return { errors };
    }

    return this.service.register(options);
  }

  @Mutation(() => UserResponse)
  async login(
    @Arg('usernameOrEmail') usernameOrEmail: string,
    @Arg('password') password: string,
  ): Promise<UserResponse> {
    const user = await this.service.findUser(usernameOrEmail);

    if (!user) {
      return {
        errors: [
          {
            field: 'usernameOrEmail',
            message: "that username doesnt't exist",
          },
        ],
      };
    }

    const valid = await argon2.verify(user.password, password);
    if (!valid) {
      return {
        errors: [
          {
            field: 'password',
            message: 'incorrect password',
          },
        ],
      };
    }

    const { id, email } = user;

    return {
      user,
      token: jwt.sign({ id, email }, SECRET_JWT_KEY),
    };
  }
}
