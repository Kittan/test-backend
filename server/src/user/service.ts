import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as argon2 from 'argon2';

import { User } from '../entities/User';
import { UsernamePasswordDTO } from '../types';

@Injectable()
export class UserService {
  constructor(@InjectRepository(User) private repository: Repository<User>) {}

  async register(options: UsernamePasswordDTO) {
    const hashedPassword = await argon2.hash(options.password);

    try {
      const user = await this.repository.create({
        ...options,
        password: hashedPassword,
      });

      return { user: await this.repository.save(user) };
    } catch (e) {
      if (e.code === '23505') {
        return {
          errors: [
            {
              field: 'username',
              message: 'username already taken',
            },
          ],
        };
      }
    }
  }

  async findUser(usernameOrEmail: string) {
    return await User.findOne({
      where: usernameOrEmail.includes('@')
        ? { email: usernameOrEmail }
        : { username: usernameOrEmail },
    });
  }
}
