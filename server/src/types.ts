import { Request, Response } from 'express';
import { Redis } from 'ioredis';
import { InputType, Field } from 'type-graphql';

import { createUserLoader } from './utils/createUserLoader';

export type MyContext = {
  req: Request;
  res: Response;
  redis: Redis;
  userLoader: ReturnType<typeof createUserLoader>;
};

export type ResultVerify = {
  id: number;
  email: string;
  iat: number;
};

@InputType()
export class UsernamePasswordDTO {
  @Field()
  email: string;
  @Field()
  username: string;
  @Field(() => String)
  password: string;
}

@InputType()
export class UserMessageDTO {
  @Field()
  userId: number;
  @Field()
  message: string;
}
