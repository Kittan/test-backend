import { Box, Button, Flex, Heading, Link } from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import NextLink from "next/link";
import { useQuery } from "@apollo/client";
import { useRouter } from "next/router";

import { MeQ } from "../graphql/queries/me";

export const NavBar = () => {
  const { data } = useQuery(MeQ, {
    fetchPolicy: "network-only",
  });

  const [isAuth, setIsAuth] = useState<string | undefined>(data?.me?.username);

  const router = useRouter();

  useEffect(() => {
    setIsAuth(data?.me?.username);
  }, [data?.me]);

  const handleLogout = () => {
    localStorage.removeItem("token");

    setIsAuth(undefined);

    router.push("/login");
  };

  return (
    <Flex position="sticky" top={0} zIndex={1} bg="#3d3d3d" p={4} height="8vh">
      <Flex w={1280} alignItems="center" m="auto">
        <NextLink href="/">
          <Link>
            <Heading color="white">TypeGraphql</Heading>
          </Link>
        </NextLink>
        <Box ml="auto">
          {!isAuth && (
            <>
              <Button
                color="black"
                mr={10}
                onClick={() => router.push("/login")}
              >
                Login
              </Button>
              <Button
                color="black"
                mr={10}
                onClick={() => router.push("/register")}
              >
                Register
              </Button>
            </>
          )}
          {isAuth && (
            <Flex color="white" alignItems="center">
              <Box color="white" fontSize={20} mr={5}>
                {data.me.username[0].toUpperCase() + data.me.username.slice(1)}
              </Box>
              <Button onClick={() => handleLogout()} color="black">
                Logout
              </Button>
            </Flex>
          )}
        </Box>
      </Flex>
    </Flex>
  );
};
