import { Box, Heading, Text } from "@chakra-ui/react";
import React from "react";
import styled from "styled-components";

const StyledMessageElement = styled(Box)`
  border-radius: 20px;
`;

interface IMessage {
  name?: string;
  message?: string;
  [x: string]: any;
}

export const MessageElement = ({ name, message, ...rest }: IMessage) => (
  <StyledMessageElement p={5} shadow="md" borderWidth="1px" {...rest}>
    <Heading fontSize="xl">{name}</Heading>
    <Text mt={4} whiteSpace="pre-wrap" overflow="auto" height="72px">
      {message}
    </Text>
  </StyledMessageElement>
);
