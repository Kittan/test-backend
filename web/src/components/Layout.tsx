import { Flex } from "@chakra-ui/react";
import React from "react";
import { NavBar } from "./NavBar";
import { Wrapper, TWrapperVariant } from "./Wrapper";

interface LayoutProps {
  variant?: TWrapperVariant;
}

export const Layout: React.FC<LayoutProps> = ({
  children,
  variant = "regular",
}) => {
  return (
    <Flex flexDirection="column" height="100%">
      <NavBar />
      <Wrapper variant={variant}>{children}</Wrapper>
    </Flex>
  );
};
