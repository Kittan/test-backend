import { Box } from "@chakra-ui/react";
import React from "react";

export type TWrapperVariant = "small" | "regular";

interface IWrapperProps {
  variant: TWrapperVariant;
}

export const Wrapper: React.FC<IWrapperProps> = ({
  children,
  variant = "regular",
}) => (
  <Box
    margin="auto"
    width={variant === "small" ? "460px" : "100%"}
    height="92vh"
    display="flex"
    flexDirection="column"
  >
    {children}
  </Box>
);
