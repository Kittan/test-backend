import React, { ChangeEvent, KeyboardEventHandler, useState } from "react";
import { Flex } from "@chakra-ui/layout";
import { Button, Textarea } from "@chakra-ui/react";
import { useMutation } from "@apollo/client";

import { AddMessageM } from "../graphql/mutations/addMessage";

interface IProps {
  userId?: number;
}

export const FormMessage = ({ userId }: IProps) => {
  const [value, setValue] = useState("");

  const [addMessage] = useMutation(AddMessageM);

  const handleInputChange = (event: ChangeEvent<HTMLTextAreaElement>) =>
    setValue(event.target.value);

  const handleSendMessage = async () => {
    const valueWithoutSpaces = value.trim().replace(/\s+/g, " ");

    if (valueWithoutSpaces) {
      try {
        await addMessage({
          variables: {
            message: value,
            userId,
          },
        });

        setValue("");
      } catch (error) {
        console.log(error);
      }
    }
  };

  const handleKeyPress = (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
    if (e.nativeEvent.key === "Enter" && !e.shiftKey) {
      handleSendMessage();
    }
  };

  return (
    <Flex height="12vh" boxShadow="0px -15px 18px -10px rgb(0 0 0 / 20%)">
      <Flex margin="auto" justifyContent="center">
        <Textarea
          value={value}
          onChange={handleInputChange}
          onKeyUp={handleKeyPress}
          placeholder="Message"
          size="md"
          resize="none"
          width="600px"
        />
        <Button
          size="xl"
          width="200px"
          border="2px"
          borderColor="blue.300"
          marginLeft="10px"
          onClick={handleSendMessage}
        >
          Send
        </Button>
      </Flex>
    </Flex>
  );
};
