import { useMutation } from "@apollo/client";
import { Box, Button } from "@chakra-ui/react";
import { Form, Formik } from "formik";
import { useRouter } from "next/router";
import React from "react";

import { InputField } from "../components/InputField";
import { Layout } from "../components/Layout";
import { RegisterM } from "../graphql/mutations/register";
import { toErrorMap } from "../utils/toErrorMap";

interface IRegisterProps {}

const Register = ({}: IRegisterProps) => {
  const router = useRouter();
  const [register] = useMutation(RegisterM);

  return (
    <Layout variant="small">
      <Box my="auto">
        <Formik
          initialValues={{ email: "", username: "", password: "" }}
          onSubmit={async (values, { setErrors }) => {
            const response = await register({ variables: { options: values } });
            if (response.data?.register.errors) {
              setErrors(toErrorMap(response.data.register.errors));
            } else if (response.data?.register.user) {
              router.push("/");
            }
          }}
        >
          {({ isSubmitting }) => (
            <Form>
              <InputField
                name="username"
                placeholder="Username"
                label="Username"
              />
              <Box mt={4}>
                <InputField name="email" placeholder="Email" label="Email" />
              </Box>
              <Box mt={4}>
                <InputField
                  name="password"
                  placeholder="Password"
                  label="Password"
                  type="password"
                />
              </Box>
              <Button
                mt={4}
                type="submit"
                justifyContent="center"
                colorScheme="blue"
                width="100%"
                isLoading={isSubmitting}
              >
                Register
              </Button>
            </Form>
          )}
        </Formik>
      </Box>
    </Layout>
  );
};

export default Register;
