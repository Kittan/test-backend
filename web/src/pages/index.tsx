import { Box, Flex, Spinner, Stack } from "@chakra-ui/react";
import React, { useCallback, useEffect, useRef, useState } from "react";
import { useVirtual } from "react-virtual";
import styled from "styled-components";
import { useLazyQuery } from "@apollo/client";
import Pusher from "pusher-js";

import { MessageElement } from "../components/MessageElement";
import { Layout } from "../components/Layout";
import { useIsAuth } from "../utils/useIsAuth";
import { MessagesQ } from "../graphql/queries/messages";
import { Message } from "../generated/graphql";
import { FormMessage } from "../components/FormMessage";

const ScrollContainer = styled(Box)`
  height: 100%;
  width: 100%;
  overflow: auto;
`;

interface IWrapperMessage {
  $virtualRow: {
    size: number;
    start: number;
  };
  $me: boolean;
}

const WrapperMessage = styled(Box)<IWrapperMessage>`
  position: absolute;
  top: 0;
  width: 30vw;
  ${({ $me }) => ($me ? "right: 0;" : "left: 0;")}
  height: ${({ $virtualRow }) => `${$virtualRow.size}px`};
  transform: ${({ $virtualRow }) => `translateY(${$virtualRow.start}px)`};
  margin-bottom: 40px;
  margin-top: 0px !important;
`;

const Index = () => {
  const [messages, setMessages] = useState<Message[]>([]);

  const parentRef = useRef<HTMLDivElement>(null);

  const { user } = useIsAuth();

  const [getMessage, { data, loading }] = useLazyQuery<{
    messages: Message[];
  }>(MessagesQ, { fetchPolicy: "network-only" });

  const rowVirtualizer = useVirtual({
    size: messages.length,
    parentRef,
    estimateSize: useCallback((e) => 180, []),
  });

  useEffect(() => {
    const pusher = new Pusher("2e8f04eee573f2ad5543", {
      cluster: "eu",
    });

    const addMessageWhenSubscribe = (data: Message) => {
      setMessages((prev) => [...prev, data]);
    };

    const channel = pusher.subscribe("chats");
    channel.bind("new-chat", addMessageWhenSubscribe);

    getMessage();

    data?.messages && setMessages(data.messages);

    return () => {
      pusher.unsubscribe("chats");
      channel.unbind("new-chat", addMessageWhenSubscribe);
    };
  }, []);

  useEffect(() => {
    data?.messages && setMessages(data.messages);
  }, [data?.messages]);

  useEffect(() => {
    parentRef.current?.scrollTo({ top: parentRef.current?.scrollHeight });
  }, [messages]);

  return (
    <Layout>
      <Flex align="center" h="80vh">
        {loading && <Spinner size="xl" />}
        <ScrollContainer
          ref={parentRef}
          style={{
            height: "100%",
            width: "100%",
            overflow: "auto",
          }}
        >
          <Stack
            spacing={8}
            style={{
              height: `${rowVirtualizer.totalSize}px`,
              margin: "auto",
              width: "100%",
              position: "relative",
            }}
          >
            {rowVirtualizer.virtualItems.map((virtualRow) => {
              const isMeMessage =
                messages[virtualRow.index].user?.id == user?.id;

              const name = isMeMessage
                ? "You"
                : messages[virtualRow.index].user?.username;

              const text = messages[virtualRow.index].message;

              return (
                <WrapperMessage
                  key={virtualRow.index}
                  $virtualRow={virtualRow}
                  $me={isMeMessage}
                >
                  <MessageElement name={name} message={text} />
                </WrapperMessage>
              );
            })}
          </Stack>
        </ScrollContainer>
      </Flex>
      <FormMessage userId={user?.id} />
    </Layout>
  );
};

export default Index;
