import { Button, Box } from "@chakra-ui/react";
import { Form, Formik } from "formik";
import { useRouter } from "next/router";
import React from "react";
import { useMutation } from "@apollo/client";

import { InputField } from "../components/InputField";
import { toErrorMap } from "../utils/toErrorMap";
import { Layout } from "../components/Layout";
import { LoginM } from "../graphql/mutations/login";

interface ILoginProps {}

const Login = ({}: ILoginProps) => {
  const router = useRouter();
  const [login] = useMutation(LoginM);

  return (
    <Layout variant="small">
      <Box my="auto">
        <Formik
          initialValues={{ usernameOrEmail: "", password: "" }}
          onSubmit={async (values, { setErrors }) => {
            const { data } = await login({ variables: values });
            if (data?.login.errors) {
              setErrors(toErrorMap(data.login.errors));
            } else if (data?.login.user) {
              if (typeof router.query.next === "string") {
                router.push(router.query.next);
              } else {
                router.push("/");
              }

              localStorage.setItem("token", data.login.token);
            }
          }}
        >
          {({ isSubmitting }) => (
            <Form>
              <InputField
                name="usernameOrEmail"
                placeholder="Username Or Email"
                label="Username Or Email"
              />
              <Box mt={4}>
                <InputField
                  name="password"
                  placeholder="Password"
                  label="Password"
                  type="password"
                />
              </Box>
              <Button
                mt={4}
                width="100%"
                type="submit"
                colorScheme="green"
                isLoading={isSubmitting}
              >
                Login
              </Button>
            </Form>
          )}
        </Formik>
      </Box>
    </Layout>
  );
};

export default Login;
