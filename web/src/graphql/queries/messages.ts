import { gql } from "@apollo/client";

import { RegularUserF } from "../fragments/RegularUser";

export const MessagesQ = gql`
  query Messages {
    messages {
      id
      createdAt
      updatedAt
      message
      user {
        ...RegularUserF
      }
    }
  }

  ${RegularUserF}
`;
