import { gql } from "@apollo/client";

import { RegularUserF } from "../fragments/RegularUser";

export const MeQ = gql`
  query Me {
    me {
      ...RegularUserF
    }
  }

  ${RegularUserF}
`;
