import { gql } from "@apollo/client";

export const RegularErrorF = gql`
  fragment RegularErrorF on FieldError {
    field
    message
  }
`;
