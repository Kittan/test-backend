import { gql } from "@apollo/client";
import { RegularErrorF } from "./RegularError";
import { RegularUserF } from "./RegularUser";

export const RegularUserResponseF = gql`
  fragment RegularUserResponse on UserResponse {
    errors {
      ...RegularErrorF
    }
    user {
      ...RegularUserF
    }
    token
  }

  ${RegularErrorF}
  ${RegularUserF}
`;
