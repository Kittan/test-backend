import { gql } from "@apollo/client";

export const RegularUserF = gql`
  fragment RegularUserF on User {
    id
    username
    email
  }
`;
