import { gql } from "@apollo/client";

import { RegularUserF } from "../fragments/RegularUser";

export const AddMessageM = gql`
  mutation AddMessage($message: String!, $userId: ID!) {
    addMessage(message: $message, userId: $userId) {
      id
      message
      user {
        ...RegularUserF
      }
    }
  }

  ${RegularUserF}
`;
