import { gql } from "@apollo/client";

import { RegularUserResponseF } from "../fragments/RegularUserReponse";

export const RegisterM = gql`
  mutation Register($options: UsernamePasswordDTO!) {
    register(options: $options) {
      ...RegularUserResponse
    }
  }

  ${RegularUserResponseF}
`;
