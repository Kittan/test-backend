import { gql } from "@apollo/client";

import { RegularUserResponseF } from "../fragments/RegularUserReponse";

export const LoginM = gql`
  mutation Login($usernameOrEmail: String!, $password: String!) {
    login(usernameOrEmail: $usernameOrEmail, password: $password) {
      ...RegularUserResponse
    }
  }

  ${RegularUserResponseF}
`;
