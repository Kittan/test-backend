import { useQuery } from "@apollo/client";
import { useRouter } from "next/router";
import { useEffect } from "react";

import { MeQ } from "../graphql/queries/me";

export const useIsAuth = () => {
  const { data, loading } = useQuery(MeQ, {
    fetchPolicy: "network-only",
  });

  const router = useRouter();

  useEffect(() => {
    if (!loading && !data?.me) {
      router.replace(`/login?next=${router.pathname}`);
    }
  }, [loading, data, router]);

  return { user: data?.me };
};
